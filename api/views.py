from django.contrib.auth.models import User, Group
from rest_framework import viewsets, filters, settings

from api.filters import UserFilter
from timetable.models import WorkDay, WorkDayType, PlanPDF
from api.serializers import UserSerializer, GroupSerializer, WorkDaySerializer, WorkDayTypeSerializer, PlanPDFSerializer


class WorkDayViewSet(viewsets.ModelViewSet):
    """
    йцуйцуйцу
    API endpoint that allows users to be viewed or edited.
    """
    queryset = WorkDay.objects.all()
    serializer_class = WorkDaySerializer
    filterset_fields = {
        'id': ['exact'],
        'user': ['exact'],
        'date': ['gte', 'lte', 'exact', 'gt', 'lt'],  # https://stackoverflow.com/a/60058800
    }


class WorkDayTypeViewSet(viewsets.ReadOnlyModelViewSet):
    """
    йцуйцуйцу
    API endpoint that allows users to be viewed or edited.
    """
    queryset = WorkDayType.objects.all()
    serializer_class = WorkDayTypeSerializer
    filterset_fields = ['id', 'name']


class PlanPDFViewSet(viewsets.ReadOnlyModelViewSet):
    """
    йцуйцуйцу
    API endpoint that allows users to be viewed or edited.
    """
    queryset = PlanPDF.objects.all()
    serializer_class = PlanPDFSerializer
    filterset_fields = ['id', 'name', 'for_month']


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filterset_class = UserFilter


class GroupViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
