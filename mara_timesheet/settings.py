import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '41k=4az^n1l9*4$bdw67*zbw%z-0vn@57gnsqq=_&n+_y9^juo'
DEBUG = True
ALLOWED_HOSTS = ['127.0.0.1', 'duty-timesheet.mhd.local', 'duty-timesheet1-python.mhd.local', 'tva.mara.local']
ROOT_URLCONF = 'mara_timesheet.urls'
WSGI_APPLICATION = 'mara_timesheet.wsgi.application'
LANGUAGE_CODE = 'ru'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, '.local/static_root')
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '.local/media_root')
LOGIN_REDIRECT_URL = '/'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'phonenumber_field',

    'crispy_forms',

    'rest_framework',
    'django_filters',
    'drf_yasg',

    'mara_timesheet',
    'timetable',
    'api',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'timesheet',
        'USER': 'timesheet',
        'PASSWORD': 'teS3zetu',
        'HOST': 'mysql-php.mhd.local',
        'PORT': '3306',
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator', },
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', },
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator', },
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator', },
]

# ------------------rest_framework---------------------https://www.django-rest-framework.org/
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ['rest_framework.permissions.IsAuthenticated'],
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend']
}
# -----------------------------------------------------

# ------------django_auth_ldap-----------https://django-auth-ldap.readthedocs.io/
import ldap
from django_auth_ldap.config import LDAPSearch, PosixGroupType
AUTHENTICATION_BACKENDS = [
    "django_auth_ldap.backend.LDAPBackend",
    "django.contrib.auth.backends.ModelBackend",
]

AUTH_LDAP_SERVER_URI = "ldap://auth.mhd.local:389"
AUTH_LDAP_BIND_DN = "uid=mara_timesheet,ou=People,dc=panbet,dc=com"
AUTH_LDAP_BIND_PASSWORD = "6kchqhiw"
AUTH_LDAP_USER_SEARCH = LDAPSearch("ou=People,dc=panbet,dc=com", ldap.SCOPE_SUBTREE, "uid=%(user)s")
AUTH_LDAP_GROUP_TYPE = PosixGroupType()
AUTH_LDAP_GROUP_SEARCH = LDAPSearch("ou=Group,dc=panbet,dc=com", ldap.SCOPE_SUBTREE)
AUTH_LDAP_USER_FLAGS_BY_GROUP = {
    "is_active": "cn=ldap_users,ou=Group,dc=panbet,dc=com",  # Разрешена авторизация
    "is_staff": "cn=admins,ou=Group,dc=panbet,dc=com",  # Доступ в алминку RO
    "is_superuser": "cn=admins,ou=Group,dc=panbet,dc=com",  # Доступ в алминку RW
}

AUTH_LDAP_USER_ATTR_MAP = {
    "first_name": "givenName",
    "last_name": "sn",
}

# AUTH_LDAP_MIRROR_GROUPS = True
# AUTH_LDAP_FIND_GROUP_PERMS = True
# AUTH_LDAP_CACHE_TIMEOUT = 3600
# ---------------------------------------


# --debug_toolbar---------------
# INTERNAL_IPS = ['127.0.0.1']
# INSTALLED_APPS += ['debug_toolbar']
# MIDDLEWARE = ['debug_toolbar.middleware.DebugToolbarMiddleware'] + MIDDLEWARE
# --------------------------------

# ----------------logger------------------------------------------------
# LOGGING = {
#     'version': 1, "disable_existing_loggers": False,
#     'filters': {
#         'require_debug_true': {'()': 'django.utils.log.RequireDebugTrue'},
#     },
#     'handlers': {
#         'stdout': {'level': 'DEBUG', 'class': 'logging.StreamHandler', "stream": "ext://sys.stdout"},
#         'stderr': {'level': 'DEBUG', 'class': 'logging.StreamHandler', "stream": "ext://sys.stderr"},
#     },
#     'loggers': {
#         '': {'level': 'DEBUG', 'handlers': ['stdout']},
#         'ldap': {'level': 'DEBUG', 'handlers': ['stderr']},
#         "django_auth_ldap": {"level": "DEBUG", "handlers": ["stderr"], 'propagate': False},
#         'django.db.backends': {'level': 'DEBUG', 'handlers': ['console']},
#         'django': {'level': 'DEBUG', 'handlers': ['stderr']},
#     }
# }
# ----------------------------------------------------------------------

# -----------------WARNING--dev-test--WARNING---------------------------
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }
# ----------------------------------------------------------------------
