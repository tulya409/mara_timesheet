from django.contrib.auth.models import User, Group
from rest_framework import serializers
from timetable.models import WorkDay, WorkDayType, Profile, PlanPDF


class WorkDaySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WorkDay
        fields = ['id', 'user', 'date', 'date_update', 'description', 'work_day_type']


class WorkDayTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WorkDayType
        fields = ['id', 'name', 'description', 'runtime', 'hidden']


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = ['user_id', 'user', 'hidden', 'phone_number', 'shift_number']


class PlanPDFSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PlanPDF
        fields = ['id', 'for_month', 'name', 'file', 'date_upload']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups', 'profile']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']
