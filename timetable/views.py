from typing import NamedTuple
from calendar import Calendar
import datetime
import json

from django.http import JsonResponse
from django.views.generic import TemplateView
from django.contrib.auth.models import User

from timetable.forms import WorkDayForm
from timetable.models import WorkDayType, WorkDay, PlanPDF


class UserWorkDay(NamedTuple):
    user: User
    work_days: list
    sum_work_days: int


class CalendarView(TemplateView):
    form_class = WorkDayForm
    template_name = 'timetable/calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        render_month = datetime.date(year=int(self.request.GET.get("year", datetime.date.today().year)),
                                     month=int(self.request.GET.get("month", datetime.date.today().month)),
                                     day=int(self.request.GET.get("day", 1)))

        month = Calendar().itermonthdates(render_month.year, render_month.month)
        month = tuple(filter(lambda _day: _day.month == render_month.month, month))

        work_day_types = WorkDayType.objects.all()
        paid_day_id = tuple(day_t.id for day_t in work_day_types if day_t.runtime > 0)

        users_work_days = []
        for user in User.objects.filter(profile__hidden=False).order_by('-profile__shift_number', 'username').select_related('profile').all():
            work_days = WorkDay.objects\
                .filter(user=user, date__year=render_month.year, date__month=render_month.month)\
                .all()
            sum_work_days = len(tuple(wd for wd in work_days if wd.work_day_type_id in paid_day_id))
            users_work_days.append(UserWorkDay(user, work_days, sum_work_days))

        plans = PlanPDF.objects\
            .filter(for_month__year=render_month.year, for_month__month=render_month.month)\
            .order_by('-date_upload')\
            .all()

        context.update({
            'render_month': render_month,
            'month': month,
            'users_work_days': users_work_days,
            'work_day_types': work_day_types,
            'plans': plans,
            'title': 'Табель',
            'opt': ''.join(f'<option value="{op.id}">{op.name}</option>' for op in work_day_types),
            **(self.extra_context or {})
        })
        return context

    def post(self, request, *args, **kwargs):
        for diff_day in json.loads(request.body):
            form = self.form_class(diff_day)
            if form.is_valid():
                form.save()
            else:
                return JsonResponse(dict(status='ERROR', msg=form.errors.as_text()), status=400)
        return JsonResponse(dict(status='OK'))
