from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def is_selected_type(context):
    work_type = context['work_type']  # type: TypeWorkShift
    day = context['day']  # type: datetime
    work_days = context['work_days']

    for work_day in work_days:
        if work_day.date == day:
            if work_type.id == work_day.work_day_type_id:
                return 'selected'
            return ''


@register.simple_tag(takes_context=True)
def get_id_work_day_type(context):
    day = context['day']

    for work_day in context['work_days']:
        if day == work_day.date:
            return work_day.work_day_type_id
