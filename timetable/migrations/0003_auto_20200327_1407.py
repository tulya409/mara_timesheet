# Generated by Django 3.0.4 on 2020-03-27 14:07

from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('timetable', '0002_auto_20200326_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='shift_number',
            field=models.PositiveSmallIntegerField(help_text='Номер смены в которую входит пользователь', null=True, verbose_name='Номер смены'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='hidden',
            field=models.BooleanField(default=False, help_text='Если установлен пользователь не будет отображаться в таблицах', verbose_name='скрытый'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='phone_number',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=13, region=None, verbose_name='телефонный номер'),
        ),
    ]
