from django import forms
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy


class UserAuthForm(AuthenticationForm):

    username = UsernameField(
        widget=forms.TextInput(attrs={'autofocus': True,
                                      'class': "form-control",
                                      'placeholder': 'Login'})
    )
    password = forms.CharField(label="Password", strip=False,
                               widget=forms.PasswordInput(
                                   attrs={'autocomplete': 'current-password',
                                          'placeholder': "Password",
                                          'class': "form-control"})
                               )


class UserLoginView(LoginView):
    success_url = reverse_lazy('index')
    redirect_authenticated_user = reverse_lazy('index')
    form_class = UserAuthForm
    template_name = 'login.html'
    redirect_field_name = 'api'
    extra_context = {'title': 'Логин'}
