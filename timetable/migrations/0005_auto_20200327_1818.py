# Generated by Django 3.0.4 on 2020-03-27 18:18

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('timetable', '0004_auto_20200327_1409'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='phone_number',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=18, region=None, verbose_name='телефонный номер'),
        ),
    ]
