from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include, reverse_lazy
from django.views.generic import RedirectView
from django.contrib.auth.views import LogoutView
from mara_timesheet.views import UserLoginView
from timetable.views import CalendarView
from django.conf import settings

urlpatterns = [
    path('', RedirectView.as_view(url=reverse_lazy('calendar')), name='index'),

    path('calendar/', login_required(CalendarView.as_view(), login_url=reverse_lazy('login')), name='calendar'),

    path('logout/', LogoutView.as_view(next_page='index'), name='logout'),
    path('login/', UserLoginView.as_view(), name='login'),

    path('api/', include('api.urls'), name='api'),

    path('admin/', admin.site.urls, name='admin'),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.views.decorators.clickjacking import xframe_options_exempt
    from django.views.static import serve
    urlpatterns += static(settings.MEDIA_URL, view=xframe_options_exempt(serve), document_root=settings.MEDIA_ROOT)

if 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
