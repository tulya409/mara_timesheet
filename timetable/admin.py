from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin

from timetable.models import Profile, WorkDayType, WorkDay, PlanPDF


@admin.register(WorkDayType)
class WorkDayTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "runtime", "hidden")


@admin.register(WorkDay)
class WorkDayAdmin(admin.ModelAdmin):
    list_display = ("user", "date", "work_day_type", "date_update", "description")
    date_hierarchy = "date"


@admin.register(PlanPDF)
class PlanPDFAdmin(admin.ModelAdmin):
    list_display = ("for_month", "file", "name", 'date_upload')
    fields = ("file", "for_month", "name")


# ------------------------------------------\/-----Очень опасная штука---------\/----------------------------

class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Доп. информация'


class MyUserAdmin(UserAdmin):
    inlines = (ProfileInline,)


admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
# -----------------------------------------/\----Очень опасная штука-------------/\----------------------

