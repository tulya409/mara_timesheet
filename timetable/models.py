from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from phonenumber_field.modelfields import PhoneNumberField


class WorkDayType(models.Model):
    """Типы смен"""
    name = models.CharField(max_length=10, verbose_name="краткое название смены")
    description = models.CharField(max_length=100, verbose_name="описание")
    runtime = models.IntegerField(verbose_name="длина смены в минутах")  # minutes
    hidden = models.BooleanField(default=False, verbose_name="скрытый")

    class Meta:
        verbose_name = "тип смены"
        verbose_name_plural = "типы смен"

    def __str__(self):
        return f"{self.name} {self.description}"


class WorkDay(models.Model):
    """Рабочие смены"""
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name="пользователь")
    date = models.DateField(verbose_name="дата смены")
    work_day_type = models.ForeignKey(WorkDayType, on_delete=models.PROTECT, verbose_name="тип смены")
    date_update = models.DateTimeField(auto_now=True, verbose_name="дата создания/обновления")
    description = models.TextField(blank=True, verbose_name="комментарий")

    def __str__(self):
        return "WorkDay( {} )".format(', '.join(
            [repr(self.user_id and self.user),
             repr(self.date),
             repr(self.work_day_type_id and self.work_day_type),
             str(self.date_update)]
        ))

    class Meta:
        verbose_name = "смена"
        verbose_name_plural = "смены"
        constraints = [models.UniqueConstraint(fields=['user', 'date'], name='unique_user_date')]


class PlanPDF(models.Model):
    """Файлы(PDF) графиков от Люды"""
    for_month = models.DateField(verbose_name='Месяц', help_text='месяц для которого этот график(день не имеет значения)')
    name = models.CharField(max_length=30, blank=True, verbose_name='отображаймое имя', help_text="имя для отображения(по умолчанию имя файла)")
    file = models.FileField(upload_to='plan_pdf', verbose_name='график в pdf', help_text='график в формате pdf который приходит на почту')
    date_upload = models.DateTimeField(auto_now_add=True, verbose_name='время создания')

    def get_view_name(self):
        return self.name or self.file.name.split('/')[-1]

    def __str__(self):
        return "{}, {}, {}".format(self.file.name, self.for_month, repr(self.name))

    class Meta:
        verbose_name = "График(PDF)"
        verbose_name_plural = "Графики(PDF)"
# --------------------\/--Магия для расширения модели пользователя--\/-------------------


class Profile(models.Model):
    """Профиль пользователя"""
    user = models.OneToOneField(User, verbose_name="пользователь", on_delete=models.CASCADE)
    hidden = models.BooleanField(default=False, verbose_name="скрытый", help_text='Если установлен пользователь не будет отображаться в таблицах')
    phone_number = PhoneNumberField(max_length=18, blank=True, verbose_name="телефонный номер")
    shift_number = models.PositiveSmallIntegerField(verbose_name="Номер смены", blank=True, null=True, help_text='Номер смены в которую входит пользователь')

    def __str__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "профиль пользователя"
        verbose_name_plural = "профили пользователей"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
# ------------------------------------------------------------------------------------
