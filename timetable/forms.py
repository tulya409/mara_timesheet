from django import forms
from timetable.models import WorkDay
from django.core.exceptions import ValidationError


class WorkDayForm(forms.ModelForm):

    class Meta:
        model = WorkDay
        fields = ('user', 'date', 'work_day_type', 'description')

    def validate_unique(self):
        try:
            self.instance.validate_unique()
        except ValidationError:
            _inst = self.Meta.model.objects.filter(user=self.instance.user, date=self.instance.date).first()
            assert _inst, 'Очень странно user и date нету в бд но поднят unique err'
            self.instance.pk = _inst.pk

