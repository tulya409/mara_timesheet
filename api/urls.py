from django.conf.urls import url, include
from rest_framework import routers

from api import views

router = routers.DefaultRouter()
router.register(r'User', views.UserViewSet)
router.register(r'Group', views.GroupViewSet)

router.register(r'WorkDay', views.WorkDayViewSet)
router.register(r'WorkDayType', views.WorkDayTypeViewSet)
router.register(r'PlanPDF', views.PlanPDFViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^ui/', include('api.swagger-ui'))
]
