function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
// ------****------****------****------****------****------****------****------****------****


$(function () {
    let select_date = document.getElementById("date_selector").getAttribute("data-select-date");
    let date_in = new Date(select_date);

    let datepicker = $('#date_selector').datepicker().data('datepicker');

    datepicker.selectDate(date_in);
    datepicker.update({
        startDate : date_in,
        dateFormat: "MM yyyy",
        view: "months",
        minView: "months",
        onSelect: function(formattedDate, date){
            location.search = "?year=" + date.getFullYear() + "&month=" + (date.getMonth() + 1)
        }
    });
    datepicker.view = 'months';
});

$(function () {
    let all_td = document.querySelectorAll("table[id=main_cal] td,th[data-date]");
    all_td.forEach(function (item) {
        let date = item.getAttribute("data-date");
        let week_number = new Date(date).getDay();
        if (week_number === 0 || week_number === 6){
            item.classList.add("date-weekend");
        }
    });
});


let diff_day = [];
$(function () {
    let mutable_days = document.querySelectorAll("td.mutable-day");
    mutable_days.forEach(function (td_day) {
        td_day.addEventListener('change', function edit_day(){

            let user_id = this.getAttribute("data-user-id");
            let date = this.getAttribute("data-date");
            let work_day_type_id = this.firstElementChild.value;

            let data = {
                'user':user_id,
                'date': date,
                'work_day_type': work_day_type_id
            };
            diff_day.push(data);
            document.getElementById("button-save").removeAttribute("hidden");})});});

$(function () {
    document.getElementById("button-save").addEventListener("click", function (){
        work_day_save();
        hidden_button_save();
    })});

function work_day_save(){
    let xhr = new XMLHttpRequest();
    xhr.open('POST', location.pathname, true);
    xhr.setRequestHeader('X-CSRFToken', csrftoken);

    xhr.onreadystatechange = function () {
        if (xhr.status === 200) {
            console.log(xhr.responseText);
            diff_day = [];
        } else{
            alert("Что то не получилось сохранить =(");
        }
    };
    xhr.send(JSON.stringify(diff_day));
}

function hidden_button_save(){
    document.getElementById("button-save").setAttribute("hidden", 'true');
    console.log(diff_day);
}

$(function () {
    document.querySelectorAll('select#work_type:not([data-wdt="None"])')
        .forEach(function (select_wdt) {
            let val = select_wdt.getAttribute('data-wdt');
            select_wdt.querySelector('[value="' + val +'"]').selected = true;
        })
});