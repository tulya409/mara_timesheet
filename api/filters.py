from django_filters import rest_framework as filters
from django.contrib.auth.models import User, Group


class UserFilter(filters.FilterSet):
    not_work_bay_day = filters.DateFilter(field_name='not_work_bay_day',
                                          label='qwe',
                                          method='filter_not_work_bay_day')

    def filter_not_work_bay_day(self, queryset, name, value):
        return queryset.filter(profile__hidden=False)\
            .exclude(workday__date=value).\
            exclude(profile__shift_number__isnull=True)

    class Meta:
        model = User
        fields = ('id', 'username')
